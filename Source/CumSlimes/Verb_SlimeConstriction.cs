﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace CumSlimes
{
	public class Verb_SlimeConstriction : Verb_MeleeApplyHediff
	{
		private readonly HediffDef constrictionHediffDef = HediffDef.Named("NCS_SlimeConstriction");

		protected override DamageWorker.DamageResult ApplyMeleeDamageToTarget(LocalTargetInfo target)
		{
			DamageWorker.DamageResult damageResult = new DamageWorker.DamageResult();
			Pawn pawn = target.Thing as Pawn;
			if (pawn == null)
			{
				Log.ErrorOnce("Attempted to apply melee hediff without pawn target", 78330053);
				return damageResult;
			}
			Hediff_SlimeConstriction constrictionHediff = (Hediff_SlimeConstriction)HediffMaker.MakeHediff(constrictionHediffDef, pawn);
			constrictionHediff.castPawn = caster;
			caster.DeSpawn();
			pawn.health.AddHediff(constrictionHediff);

			damageResult.AddHediff(constrictionHediff);
			return damageResult;
		}

        public override bool IsUsableOn(Thing target)
        {
			if(!(target is Pawn pawn))
            {
				return false;
            }
            if(!base.IsUsableOn(target))
            {
				return false;
            }
			return !pawn.health.hediffSet.HasHediff(constrictionHediffDef);
        }
    }
}
