﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace CumSlimes
{
    [HarmonyPatch(typeof(TaleUtility), "Notify_PawnDied")]
    public class Patch_TaleUtility
    {
        static ThingDef cumSlimeRace = ThingDef.Named("NCS_CumSlime");

        [HarmonyPrefix]
        public static bool SkipPawnKilledIfCumSlime(Pawn victim)
        {
            if (victim.def == cumSlimeRace)
            {
                return false;   // skip everything related to pawns dying, cumslimes are worth nothing
            }
            return true;
        }
    }

    [HarmonyPatch(typeof(PawnDiedOrDownedThoughtsUtility), "GetThoughts")]
    public class Patch_PawnDiedOrDownedThoughtsUtility
    {
        static ThingDef cumSlimeRace = ThingDef.Named("NCS_CumSlime");

        [HarmonyPrefix]
        public static bool SkipPawnThoughtsIfCumSlime(Pawn victim)
        {
            if (victim.def == cumSlimeRace)
            {
                return false;   // skip everything related to pawns dying, cumslimes are worth nothing
            }
            return true;
        }
    }
}
