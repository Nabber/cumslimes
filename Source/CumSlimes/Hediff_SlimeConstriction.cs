﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace CumSlimes
{
    public class Hediff_SlimeConstriction : HediffWithComps
    {
        public Thing castPawn;
#if v1_4
        public override void Notify_PawnDied()
        {
            base.Notify_PawnDied();
#else
        public override void Notify_PawnDied(DamageInfo? dinfo, Hediff culprit = null)
        {
            base.Notify_PawnDied(dinfo, culprit);
#endif
            if (pawn.MapHeld != null)
            {
                GenSpawn.Spawn(castPawn, pawn.Position, pawn.MapHeld);
            }
        }

        public override void PostRemoved()
        {
            base.PostRemoved();
            if(pawn.MapHeld != null)
            {
                GenSpawn.Spawn(castPawn, pawn.Position, pawn.MapHeld);
            }
        }
    }
}
